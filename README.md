# UniversalResolver #
Upgrade your old project's dependencies to SBT or Maven. 
UniversalResolver resolves all the jar dependencies in your project and tries several possibilities to generate a working SBT build.sbt and Maven pom.xml.

It resolves all jars and tries to figure out the correct version based on SHA checksum, classpath or filename. Then it tries to compile the sources until it finds a working configuration. Custom tasks to execute per dependency configuration can be given too.

1. resolve jars on maven central per SHA checksum or classname imported in source files
2. store resolved dependencies in tmp file
3. find all reasonable configurations of the resolved dependencies
4. try to build each dependency configuration until one finishes

## Usage ##


```
#!bash
UniversalResolver 1.0
Usage: UniversalResolver [options]

  -s <srcpath1>,<srcpath2>... | --srcPaths <srcpath1>,<srcpath2>...
        required src paths to include
  -j <jar1>,<jar2>... | --jars <jar1>,<jar2>...
        required jars/jar paths to include
  -t /path/To/Dir | --testDirectory /path/To/Dir
        required directory where test configurations will be stored
  -a <task1>,<task2>... | --sbt-tasks <task1>,<task2>...
        SBT Tasks to be executed. i.e. compile
  -d /path/To/dependencyFile.json | --dependencyFile /path/To/dependencyFile.json
        optional file where the dependency buffer will be stored
  -l | --search
        load and search dependencies from remote repositories
  -g | --generateConfigurations
        generate dependency configurations
  -c <value> | --findByNameCount <value>
        number of dependencies to resolve by class name per jar
```
## Example ##
This will resolve the jars in TestProject/lib/ and will compile the sources in  TestProject/src/. All tried configurations will be stored in testDir/. The program terminates when a working configuration (in this case determined by `sbt compile`) was found or all configruations have been tried.

```
#!bash

java -jar resolver.jar -s TestProject/src/ -j TestProject/lib/ -t testDir/  -a compile

```

The generated build.sbt will look like this: 
```
#!scala

name := "generatedBuild"

version := "1.0"

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"



libraryDependencies ++= Seq(
       /*MavenDependency(UnmanagedDependency(/home/nyxos/IdeaProjects/UniversalResolverTestProject/lib/xmlbeans-5.3.0-rc1.jar),org.ow2.jonas.osgi,xmlbeans,5.3.0-RC1)*/ "org.ow2.jonas.osgi" % "xmlbeans" % "5.3.0-RC1" , 
/*MavenDependency(UnmanagedDependency(/home/nyxos/IdeaProjects/UniversalResolverTestProject/lib/apache-ant-1.8.2.jar),org.apache.ant,ant,1.8.2)*/ "org.apache.ant" % "ant" % "1.8.2" , 
/*MavenDependency(UnmanagedDependency(/home/nyxos/IdeaProjects/UniversalResolverTestProject/lib/commons-io-2.2.jar),commons-io,commons-io,2.4)*/ "commons-io" % "commons-io" % "2.4" 
)
unmanagedSourceDirectories in Compile ++= Seq(
              file("/home/nyxos/IdeaProjects/UniversalResolverTestProject/src")
)
```
and the pom.xml like this: 


```
#!maven

<?xml version='1.0' encoding='UTF-8'?>
<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0">
    <modelVersion>4.0.0</modelVersion>
    <groupId>generatedbuild</groupId>
    <artifactId>generatedbuild_2.10</artifactId>
    <packaging>jar</packaging>
    <description>generatedBuild</description>
    <version>1.0</version>
    <name>generatedBuild</name>
    <organization>
        <name>generatedbuild</name>
    </organization>
    <dependencies>
        <dependency>
            <groupId>org.scala-lang</groupId>
            <artifactId>scala-library</artifactId>
            <version>2.10.4</version>
        </dependency>
        <dependency>
            <groupId>org.ow2.jonas.osgi</groupId>
            <artifactId>xmlbeans</artifactId>
            <version>5.3.0-RC1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.ant</groupId>
            <artifactId>ant</artifactId>
            <version>1.8.2</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.4</version>
        </dependency>
    </dependencies>
    <repositories>
        <repository>
            <id>TypesafeRepo</id>
            <name>Typesafe Repo</name>
            <url>http://repo.typesafe.com/typesafe/releases/</url>
            <layout>default</layout>
        </repository>
    </repositories>
</project>
```