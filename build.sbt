name := "UniversalResolver"

version := "1.0"

scalaVersion := "2.11.6"

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += Resolver.sonatypeRepo("public")

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-json" % "2.4.0-M2"
  , "org.scalaj" %% "scalaj-http" % "1.1.4"
  , "org.scala-lang.modules" %% "scala-pickling" % "0.10.1"
  , "commons-io" % "commons-io" % "2.4"
  , "com.github.scopt" %% "scopt" % "3.3.0"
)

//-Xlog-implicits
scalacOptions ++= Seq("-Xlog-implicits")

unmanagedJars in Compile ++= Seq(
  Attributed(file("bla"))(AttributeMap())
)

conflictManager := ConflictManager.latestRevision

excludeFilter in unmanagedSources := HiddenFileFilter || "*/testDir*"