package de.ax.universalResolver

import java.io.{FileWriter, File}

import scala.io.Source

import java.io.{FileReader, FileWriter, FileInputStream, File}
import java.nio.file.{Files, Paths}
import java.util.Scanner
import java.util.zip.GZIPInputStream

import org.apache.commons.io.FileUtils
import play.api.libs.json.Json

import scala.io.Source
import scala.util.{Failure, Success, Try}
import scalaj.http.Http
import play.api.libs.json._
import scala.pickling._, Defaults._, json._
import scala.pickling._
import Defaults._
import json._
import scala.pickling.shareNothing._
import scala.collection.JavaConversions._

/**
 * Created by nyxos on 08.06.15.
 */
class DependencyBuffer(resolver: Option[PartialFunction[Dependency, Dependency]], config: Config) {
println("new DependencyBuffer")
  val jarFiles = config.jars.flatMap { f =>
    if (f.isDirectory)
      f.listFiles().toList.filter(x => x.isFile && x.getAbsolutePath.endsWith(".jar"))
    else
      List(f)
  }
  println("config jars: " + config.jars)
  println("found jars: " + jarFiles)

  def storeDependencies(resolvedDependencies: Option[List[Dependency]], f: File) {
    resolvedDependencies match{
      case Some(deps)=>
        println(s"writing ${deps.size} dependencies to ${f.getAbsolutePath}!")
        val writer = new FileWriter(f)
        try {
          writer.write(deps.pickle.value)
        } finally {
          writer.flush()
          writer.close()
        }
      case _ => println("no dependencies to store!")
    }
  }

  def loadDependencies(f: File): Stream[Dependency] = {
    println(s"loading dependencies from ${f.getAbsolutePath}...")
    Source.fromFile(f).mkString.unpickle[List[Dependency]].toList.toStream
  }

  val bufferFile = new File("dependencies.json")
  lazy val resolvedDependenciesDirect: Option[Stream[Dependency]] = resolver.map(r => jarFiles.toStream.map(f => UnmanagedDependency(f.getPath)).map(r))
  lazy val resolvedDependenciesBuffered: Option[Stream[Dependency]] = {
    if (config.useBuffer) {
      if (!bufferFile.exists()) {
        storeDependencies(resolvedDependenciesDirect.map(_.toList), bufferFile)
      }
      Some(loadDependencies(bufferFile))
    } else {
      resolvedDependenciesDirect
    }
  }
}
