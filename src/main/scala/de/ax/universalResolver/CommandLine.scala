package de.ax.universalResolver

import java.io.File

/**
 * Created by nyxos on 08.06.15.
 */
case class CommandLine(args: Seq[String]) {
  val parser = new scopt.OptionParser[Config]("UniversalResolver") {
    override def showUsageOnError = true

    head("UniversalResolver", "1.0")
    opt[Seq[File]]('s', "srcPaths") valueName ("<srcpath1>,<srcpath2>...") required() action { (x, c) =>
      c.copy(srcPaths = x.toList)
    } text ("required src paths to include")
    opt[Seq[File]]('j', "jars") valueName ("<jar1>,<jar2>...") required() action { (x, c) =>
      c.copy(jars = x.toList)
    } text ("required jars/jar paths to include")
    opt[Seq[File]]('z', "additionalJars") valueName ("<jar1>,<jar2>...") required() action { (x, c) =>
      c.copy(additionalJars = x.toList)
    } text ("required jars/jar paths to include but not resolve")
    opt[File]('t', "testDirectory") valueName ("/path/To/Dir") required() action { (x, c) =>
      c.copy(testDirectory = x)
    } text ("required directory where test configurations will be stored")
    opt[Seq[String]]('a', "sbt-tasks") valueName ("<task1>,<task2>...") optional() action { (t, c) =>
      c.copy(task = t.mkString(" "))
    } text ("SBT Tasks to be executed. i.e. compile")
    opt[File]('d', "dependencyFile") valueName ("/path/To/dependencyFile.json") optional() action { (x, c) =>
      c.copy(dependencyFile = x)
    } text ("optional file where the dependency buffer will be stored")
    opt[Unit]('l', "search") action { (_, c) =>
      c.copy(load = true)
    } text ("load and search dependencies from remote repositories")
    opt[Unit]('n', "no-chache") action { (_, c) =>
      c.copy(useBuffer = false)
    } text ("load and search dependencies from remote repositories")
    opt[Unit]('g', "generateConfigurations") action { (_, c) =>
      c.copy(generateConfigurations = true)
    } text ("generate dependency configurations")
    opt[Unit]('r', "tryDependenciesSingle") action { (_, c) =>
      c.copy(tryDependenciesSingle = true)
    } text ("tryDependenciesSingle")
    opt[Int]('c', "findByNameCount") optional() action { (n, c) =>
      c.copy(findByNameCount = n)
    } text ("number of dependencies to resolve by class name per jar")
  }
  // parser.parse returns Option[C]
  val conf = parser.parse(args, Config())
}

case class Config(testDirectory: File = new File("../testDir"),
                  dependencyFile: File = new File("dependencies.json"),
                  srcPaths: List[File] = List.empty[File],
                  jars: List[File] = List.empty[File],
                  additionalJars: List[File] = List.empty[File],
                  load: Boolean = true,
                  tryDependenciesSingle:Boolean=false,
                  generateConfigurations: Boolean = true,
                  compile: Boolean = true,
                  task: String = "compile",
                  useBuffer: Boolean = true,
                  findByNameCount: Int = 3
                   )
