package de.ax.universalResolver

import java.io.File
import java.nio.file.{Files, Paths}
import java.util.regex.{Pattern, Matcher}

import play.api.libs.json.{JsArray, Json}

import scala.Predef
import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}
import scalaj.http.Http

/**
 * Created by nyxos on 08.06.15.
 */
class DependencyResolver(config: Config) {
  println("new dependency resolver")
  val ch: Option[ConfigurationHandler] = if(config.tryDependenciesSingle)
    Some(new ConfigurationHandler(Config()))
  else
    None
  val resolveSha: PartialFunction[Dependency, Dependency] = {
    case originDependency@UnmanagedDependency(file) =>
      val md = java.security.MessageDigest.getInstance("SHA-1")
      println(s"resolving by SHA $file")

      def getSha(filePath: String): String = {
        val byteArray = Files.readAllBytes(Paths.get(filePath))
        md.digest(byteArray).map("%02x".format(_)).mkString
      }
      val dep = Try {
        val sha = getSha(file)
        val httpResponseString = Http("http://search.maven.org/solrsearch/select").params(
          //          "q" -> "1:\"35379fb6526fd019f331542b4e9ae2e566c57933\"",
          "q" -> s"""1:"$sha"""",
          "rows" -> "2",
          "wt" -> "json"
        ).asString.body
        val json = Json.parse(httpResponseString)
        println(Json.prettyPrint(json))
        val response = json \ "response"
        val docs = response \ "docs"
        val found = for {i <- 0 to (response \ "numFound").as[Int] - 1
                         doc = docs(i)
        } yield {

            println(Json.prettyPrint(doc))
            val g = doc \ "g"
            val a = doc \ "a"
            val v = doc \ "v"
            MavenDependency(s"resolved by SHA '$sha'", originDependency,
              g.as[String],
              a.as[String],
              v.as[String]
            )
          }
        found.find(d => ch.flatMap(_.configurationHandler(Stream(Stream(d))).headOption).map(_.success).getOrElse(true))
      }
      (dep match {
        case Success(Some(mavenDependency)) => Some(mavenDependency)
        case Failure(e) =>
          println("Error during sha1 resolve: " + e + " " + e.getStackTrace.toList.mkString("\n"))
          None
        case _ => None
      }).getOrElse(originDependency)
  }


  lazy val imports: List[String] = {
    val files = config.srcPaths.flatMap { p =>
      def loop(p: File): Stream[File] = {
        println(s"inside: " + p)
        if (p.isDirectory) {
          p.listFiles().toStream.flatMap(loop)
        } else {
          Stream(p)
        }
      }
      loop(p)
    }
    val p = Pattern.compile("import\\s+((\\w+.)+(\\w+|\\*));")
    val time = System.currentTimeMillis()
    val foundImports: List[String] = files.toStream.flatMap { f =>
//      println(s"searching imports in ${f}")
      val s = new Predef.String(Files.readAllBytes(f.toPath), "UTF-8")
      val m = p.matcher(s)
      val imps = new ListBuffer[String]
      val run = new Thread {
        override def run(): Unit = {
          var i = 0
          while (m.find()) {
            i += 1
            imps.append(m.group(1))
          }
        }
      }
      run.start()
      val t = System.currentTimeMillis()
      while (System.currentTimeMillis() - t < 100) {
        Thread.sleep(10)
      }
      run.stop()
      println(s"found ${imps.size} imports in ${f.getAbsolutePath}")
      imps.toList
    }.takeWhile(x => (System.currentTimeMillis() - time) < 30000).toList
    println(s"found ${foundImports.size} imports")
    foundImports
  }


  val resolveByFile: PartialFunction[Dependency, Dependency] = {
    case originDependency@UnmanagedDependency(file) =>
      import java.io.FileInputStream
      import java.util.zip.ZipInputStream
      println(s"resolving by class $file")
      val zip = new ZipInputStream(new FileInputStream(file))
      var e = zip.getNextEntry
      var names: List[String] = List.empty[String]
      while (e != null) {
        names ::= e.getName
        e = zip.getNextEntry
      }

      def dep(nameOrigin: String) = Try {
        val name=nameOrigin.replaceAll("\\.$","")
        val httpResponseString = Http("http://search.maven.org/solrsearch/select").params(
          "q" -> s"""fc:$name""",
          "rows" -> "20",
          "wt" -> "json"
        ).asString.body
        val json = Json.parse(httpResponseString)
        println(Json.prettyPrint(json))
        val response = json \ "response"
        val docs = response \ "docs"
        val found = docs match {
          case JsArray(vals) =>
            for (doc <- vals) yield {

              println(Json.prettyPrint(doc))
              val g = doc \ "g"
              val a = doc \ "a"
              val v = doc \ "v"
              MavenDependency(s"resolved by class reference '$name'", originDependency,
                g.as[String],
                a.as[String],
                v.as[String]
              )

            }
        }
        found.toList
      } match {
        case s@Success(_) =>
          println("success! " + s)
          s
        case f@Failure(_) =>
          println("failure! " + f)
          f
      }
      val searching = new File(file).getName.toLowerCase.replaceAll("\\.jar$", "").replaceAll("([^a-z\\-.0-9])|(\\d+\\.\\d+)", "").split("[\\-.]").toList
      val goodNames = names.sortBy(n => -n.split("/").length).map(_.replaceAll("/", ".").replaceAll("\\.class", "")).filter(!_.contains("$")).toStream



      val imported = goodNames.filter { f =>
        val found: Option[String] = imports.find(i => {
//          println(s"searching $i in  $f")
          if (i.endsWith("*")) f.toLowerCase.startsWith(i.replaceAll("\\*", "").toLowerCase) else i.toLowerCase == f.toLowerCase
        })
        found.isDefined
      }

      val foundJarName = goodNames.filter { f =>

//        println("searching " + searching + " in " + f)
        searching.exists(f.toLowerCase.contains)
      }
      println(s"IMPORTS: $imports")
      println(s"goodNames: ${goodNames.toList}")
      println(s"IMPORTED: ${imported.toList}")

      val succ: Stream[Try[Stream[MavenDependency]]] = imported.union(foundJarName).++(imported).++(foundJarName).++(imported).toStream.filter(n=> n.length>10).map(name => dep(name)).take(20).filter(x => x.isSuccess && x.map(_.nonEmpty).getOrElse(false)).map(t => t.map(deps => {
        val depsStream = deps.toStream.take(config.findByNameCount)
        if (ch.flatMap(_.configurationHandler(Stream(depsStream)).headOption).exists(_.success)) {
          depsStream.filter(d => ch.flatMap(_.configurationHandler(Stream(Stream(d))).headOption).map(_.success).getOrElse(true))
        } else {
          depsStream
        }
      })).take(config.findByNameCount)



      val flatten = succ.flatMap {
        case Success(seq) => seq
        case Failure(err) =>
          println("Error during resolve: " + err + " " + err.getStackTrace.toList.mkString("\n"))
          Stream.empty[Dependency]
      }.toSet
      if (flatten.isEmpty) {
        originDependency
      } else {
        DependencyOptions(flatten.toList)
      }
    case x => x
  }

  var noop: PartialFunction[Dependency, Dependency] = {
    case x => x
  }


  val resolveStrategies: List[PartialFunction[Dependency, Dependency]] = List(
    resolveSha,
    resolveByFile
  )
  val resolver: Option[PartialFunction[Dependency, Dependency]] = {
    println(s"loading ${resolveStrategies.size} resolver strategies...")
    (if (config.load) resolveStrategies else List(noop)).headOption.map(head => resolveStrategies.tail.foldLeft(head)((m, s) => m.andThen(s)))

  }


}
