package de.ax.universalResolver

import java.io.{FileWriter, File}
import java.util.{Date, Scanner}

import scala.StringBuilder
import scala.util.Try

/**
 * Created by nyxos on 08.06.15.
 */
class ConfigurationHandler(config: Config) {
  println("new ConfigurationHandler")
  val configurationHandler: PartialFunction[Stream[Stream[Dependency]], Stream[ExecutionResult]] = {
    case confs: Stream[Stream[Dependency]] => {
      //      println("Dependencies: " + confs.map(conf => conf.mkString("\n\t")).mkString("\n\t", "\n\n--------------------------------\nDependency Configuration:\n\t", ""))
      val testDir = config.testDirectory
      testDir.mkdirs()
      confs.toStream.map { conf =>
        println(s"trying to build: $conf")
        val dependencies = conf.filter(_.isInstanceOf[MavenDependency])
        val libraryDependencies = dependencies.map {
          case md@MavenDependency(_, from, g, a, v) =>
            s"""/*$md*/ \n\t"$g" % "$a" % "$v" """
        }.mkString(", \n")
        val unmanagedDependencies = conf.filter(_.isInstanceOf[UnmanagedDependency])
        //
        val additionalJarFiles = config.additionalJars.flatMap { f =>
          if (f.isDirectory)
            f.listFiles().toList.filter(x => x.isFile && x.getAbsolutePath.endsWith(".jar"))
          else
            List(f)
        }
        val unmanagedJars = (unmanagedDependencies.map {
          case ud@UnmanagedDependency(from) =>
            s"""/*$ud*/  \n\tAttributed(file("${from.replace("\\", "\\\\")}"))(AttributeMap())"""
        } ++ additionalJarFiles.toStream.map(jf=>  s"""/*additionalJar*/  \n\tAttributed(file("${jf.getAbsolutePath.replace("\\", "\\\\")}"))(AttributeMap())""")).mkString(", \n")

        val unmanagedSrcs = config.srcPaths.map(s => "file(\"" + s.getAbsolutePath.replace("\\", "\\\\") + "\")").mkString(", \n")
        val content = s"""
name := "generatedBuild"

version := "1.0"

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"


""" + {
          if (libraryDependencies.nonEmpty) {
            s"""

libraryDependencies ++= Seq(
$libraryDependencies
)

              """
          } else {
            ""
          }
        } + {
          if (unmanagedDependencies.nonEmpty||additionalJarFiles.nonEmpty) {
            s"""

unmanagedJars in Compile ++= Seq(
$unmanagedJars
)

              """
          } else {
            ""
          }
        } + {
          if (config.srcPaths.nonEmpty) {
            s"""

unmanagedSourceDirectories in Compile ++= Seq(
$unmanagedSrcs
)

              """
          } else {
            ""
          }
        }
        val build: String = s"build_${new Date().getTime}_${content.hashCode.toString}"
        val buildDir = new File(testDir.getAbsoluteFile, build)
        buildDir.mkdirs()
        val buildFile = new File(buildDir, "build.sbt")
        val writer = new FileWriter(buildFile)
        try {
          writer.write(content)
        } finally {
          writer.flush()
          writer.close()
        }
       val er=executeSbtTask(buildDir, config.task)
        if(er.success&&dependencies.size>1){
          executeSbtTask(buildDir,"make-pom")
        }
        er.copy(id=Some(build),dir = Some(buildDir.getAbsolutePath))
      }
    }
  }

  def executeSbtTask(buildDir: File, task: String) = {
    val b = new ProcessBuilder("C:\\Program Files (x86)\\sbt\\bin\\sbt.bat", task)
    b.directory(buildDir)
    b.redirectErrorStream(true)
    val process = b.start()
    val s = new Scanner(process.getInputStream)
    val textBuilder = new StringBuilder()
    println("waiting")
    while (s.hasNextLine) {
      val line = s.nextLine()
      println(line)
      textBuilder.append(line)
      textBuilder.append("\n")
    }
    val text = textBuilder.toString()
    println("done")
    s.close()
    var done = false
    val t = new Thread() {
      override def run() = {
        try {
          process.waitFor()
        }
        finally {
          done = true
        }
      }
    }
    t.start()
    val time = System.currentTimeMillis()
    while (!done || (System.currentTimeMillis() - time) < 1000) {
      Thread.sleep(10)
    }


    ExecutionResult(process.exitValue() == 0 && !text.contains("[error]"), text)
  }
}

case class ExecutionResult(success: Boolean, text: String, id:Option[String]=None,dir:Option[String]=None)