package de.ax.universalResolver

/**
 * Created by nyxos on 07.06.15.
 */
sealed trait Dependency

case class UnmanagedDependency(file: String) extends Dependency


case class MavenDependency(method: String, from: UnmanagedDependency, group: String, artifact: String, version: String) extends Dependency

case class DependencyOptions(options: List[Dependency]) extends Dependency


