package de.ax.universalResolver

import java.io._
import java.nio.file.{Files, Paths}
import java.util.Scanner
import java.util.regex.Pattern
import java.util.zip.GZIPInputStream

import org.apache.commons.io.FileUtils
import play.api.libs.json.Json

import scala.io.Source
import scala.util.{Failure, Success, Try}
import scalaj.http.Http
import play.api.libs.json._
import scala.pickling._, Defaults._, json._
import scala.pickling._
import Defaults._
import json._
import scala.pickling.shareNothing._
import scala.collection.JavaConversions._

/**
 * Created by nyxos on 02.06.15.
 */
object Main extends App {


  private val line = CommandLine(args)
  println(s"CONFIG: ${line.conf}")
  println(line.conf.map { config =>
    val resolver = new DependencyResolver(config).resolver

    val buffer = new DependencyBuffer(resolver, config)
    val configurator = new DependencyConfigurator
    val manager = new ConfigurationHandler(config)
    line.parser.showUsage
    val resultBuffer = collection.mutable.ListBuffer.empty[ExecutionResult]
    buffer.resolvedDependenciesBuffered.map(configurator.configurator).map(x => manager.configurationHandler(x)).map { ers =>
      ers.map { e =>
        resultBuffer += e
        resultBuffer.sortBy { e =>
          getErrorCount(e)
        }
        val listing = resultBuffer.map { r =>
          s"${getErrorCount(r)} ${r.success} ${r.id}"
        }.mkString("\n")
        val resFile = new File("results.txt")
        val fw = new FileWriter(resFile)
        fw.write(listing)
        fw.close()
        val listingExtended = resultBuffer.map { r =>
          s"${getErrorCount(r)} ${r.success} ${r.id} -----------------------------------------------------\n${r.text}"
        }.mkString("\n\n##########################################################################\n\n")
        val resFile2 = new File("resultsExtended.txt")
        val fw2 = new FileWriter(resFile2)
        fw2.write(listingExtended)
        fw2.close()
        println(listing)
        e
      }.find(_.success).map { e =>
        val additionalJarFiles = config.additionalJars.flatMap { f =>
          if (f.isDirectory)
            f.listFiles().toList.filter(x => x.isFile && x.getAbsolutePath.endsWith(".jar"))
          else
            List(f)
        }
        val plugins = <build>
          <plugins>
            <plugin>
              <artifactId>maven-compiler-plugin</artifactId>
              <version>3.2</version>
              <configuration>
                <source>1.7</source>
                <target>1.7</target>
              </configuration>
            </plugin>
            <plugin>
              <groupId>org.codehaus.mojo</groupId>
              <artifactId>build-helper-maven-plugin</artifactId>
              <version>1.1</version>
              <executions>
                <execution>
                  <id>add-source</id>
                  <phase>generate-sources</phase>
                  <goals>
                    <goal>add-source</goal>
                  </goals>
                  <configuration>
                    <sources>
                      {config.srcPaths.map { p => <source>
                      {p.getAbsolutePath.replaceAll("\\\\", "/")}
                    </source>
                    }}
                    </sources>
                  </configuration>
                </execution>
              </executions>
            </plugin>
          </plugins>
        </build>.mkString("\n\t")
        val additionalDependencies = additionalJarFiles.map { j => {
          <dependency>
            <groupId>gus.{j.getName.replaceAll("\\W+", "")}</groupId>
            <artifactId>
              {j.getName.replaceAll("\\W+", "")}</artifactId>
            <version>0.1</version>
            <scope>system</scope>
            <systemPath>{j.getAbsolutePath.replaceAll("\\\\", "/")}</systemPath>
          </dependency>
        }
        }.mkString("\n\t")
        e.dir.map(new File(_)).flatMap { f =>
          new File(f, "target").listFiles.find(_.getName.startsWith("scala")).flatMap(f => f.listFiles().find(pom => pom.getName.endsWith("pom")))
        }.foreach { f =>
          val target = new File(f.getParentFile.getParentFile.getParentFile, "pom.xml")
          val fw = new FileWriter(target)
          println(s"additionalDependencies: $additionalDependencies")
          fw.write( Source.fromFile(f).mkString.replaceAll("<dependencies>", s"$plugins<dependencies>\n$additionalDependencies\n"))
          fw.flush()
          fw.close()
        }
        e
      }
    }
  }.getOrElse("No Config!"))

  def getErrorCount(e: ExecutionResult): Int = {
    "\\[error\\] +(\\d+) +errors".r.findAllMatchIn(e.text).toList.headOption.flatMap(m => Option(m.group(1))).map(_.toInt).getOrElse(-1)
  }
}


