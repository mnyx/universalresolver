package de.ax.universalResolver

/**
 * Created by nyxos on 08.06.15.
 */
class DependencyConfigurator {
  println("new DependencyConfigurator")
  val configurator: PartialFunction[Stream[Dependency], Stream[Stream[Dependency]]] = {
    case deps =>
      val c: Stream[Stream[Dependency]] = deps.map {
        case DependencyOptions(os) => os.sortBy((x: Dependency) =>
          x match {
            case MavenDependency(_, _, _, _, v) => Option(v).getOrElse("")
            case _ => ""
          }
        ).reverse.toStream
        case dep => Stream(dep)
      }
      val x: Stream[Stream[Dependency]] = c.toList.foldLeft(Stream.fill(1)(Stream.empty[Dependency])) {
        (l, ss: Stream[Dependency]) =>

          l.flatMap(ds => ss.map {
            d =>
              println(s"adding $d to $ds")
              d +: ds
          })
      }

      x
  }
}
